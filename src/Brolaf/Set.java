package Brolaf;
import java.io.File;
import java.io.IOException;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;


public class Set implements CommandExecutor{

	public File file = new File("plugins/Brolaf", "loc.yml");
	public FileConfiguration cfg = YamlConfiguration.loadConfiguration(this.file);
	public boolean onCommand(CommandSender sender, Command cmd, String label,String[] args) {
		Player p = (Player)sender;
		if (cmd.getName().equalsIgnoreCase("setcount")){
			if (args.length == 0){
				Location loc = p.getLocation();
			    this.cfg.set("world" , p.getWorld().getName());
			    this.cfg.set("x", loc.getX());
			    this.cfg.set("y", loc.getY());
			    this.cfg.set("z", loc.getZ());
			    this.cfg.set("yaw", loc.getYaw());
			    this.cfg.set("pitch", loc.getPitch());
			    try {
			    	
					this.cfg.save(this.file);
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "Set"));
				} catch (IOException e) {
					e.printStackTrace();
				}
			    return true;
			}
			
		}
		
		
		
		
		return true;
	}

}
