package Brolaf;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInventoryEvent;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class JoinDeath implements Listener{

	private Brolaf plugin;
	public JoinDeath(Brolaf plugin){
		this.plugin = plugin;
	}
	private Scoreboard board;
	@EventHandler
	public void onjoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		if (!e.getPlayer().hasPotionEffect(PotionEffectType.HEALTH_BOOST)){
			p.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 3000000 , 4));
			p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 2 , 200));
		}
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		Scoreboard board = manager.getNewScoreboard();
		Objective objective = board.getObjective("xy");
	    
		Score score = objective.getScore(p);
		if (score.getScore() == 1){
			p.setFlying(true);
			p.setGameMode(GameMode.ADVENTURE);
			p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 3000000 , 200));
			p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 3000000 , 200));
		}
		
		
		
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		final Player p = e.getEntity().getPlayer();
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		Scoreboard board = manager.getNewScoreboard();
		Objective objective = board.registerNewObjective("xy", "dummy");
		Score score = objective.getScore(p);
		score.setScore(1);
		p.setAllowFlight(true);
		
		p.setGameMode(GameMode.ADVENTURE);
		p.removePotionEffect(PotionEffectType.WEAKNESS);
		p.removePotionEffect(PotionEffectType.INVISIBILITY);
		p.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
		p.removePotionEffect(PotionEffectType.SATURATION);
		p.removePotionEffect(PotionEffectType.SLOW_DIGGING);
		p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 3000000 , 200));
		p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 3000000 , 200));
		p.setHealth(20);
		p.setFoodLevel(20);
		p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 1, 2));
		p.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, 1, 100000));
		p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 1, 100000000));
		p.setHealth(20);
		p.setFoodLevel(20);
		Bukkit.getScheduler().scheduleSyncDelayedTask(this.plugin, new Runnable(){

			public void run() {
				p.setFlying(true);
				
			}
			
		}, 3*20L);
		
		p.setVelocity(p.getVelocity().setY(3));
		p.getInventory().clear();
		p.setFoodLevel(20);
		p.setFireTicks(0);
		for (Player on : Bukkit.getOnlinePlayers()){
			on.playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 1);
			on.playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 1);
			on.playEffect(p.getLocation(), Effect.ENDER_SIGNAL, 1);
			on.playSound(p.getLocation(), Sound.SUCCESSFUL_HIT, 1, 1);
		}
		
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		Player p = e.getPlayer();
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		Scoreboard board = manager.getNewScoreboard();
		Objective objective = board.getObjective("xy");
		Score score = objective.getScore(p);
		if (score.getScore() == 1){
			e.setCancelled(true);
		} else {
			e.setCancelled(false);
		}
		
		
	}
	@EventHandler
	public void onPlace(BlockPlaceEvent e){
		Player p = e.getPlayer();
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		Scoreboard board = manager.getNewScoreboard();
		Objective objective = board.getObjective("xy");
		Score score = objective.getScore(p);
		if (score.getScore() == 1){
			e.setCancelled(true);
		} else {
			e.setCancelled(false);
		}
	}
	@EventHandler
	public void gm(PlayerGameModeChangeEvent e){
		
		Player p = e.getPlayer();
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		Scoreboard board = manager.getNewScoreboard();
		Objective objective = board.getObjective("xy");

		Score score = objective.getScore(p);
		if (score.getScore() == 1){
			p.setGameMode(GameMode.ADVENTURE);
		} else {
			p.setGameMode(GameMode.SURVIVAL);
		}
		
		
	}
	/*
	@EventHandler
	public void inv(InventoryClickEvent e){
		Player p = (Player) e.getView();
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		Scoreboard board = manager.getNewScoreboard();
		Objective objective = board.getObjective("xy");
		Score score = objective.getScore(p);
		if (score.getScore() == 1){
			e.setCancelled(true);
		} else {
			e.setCancelled(false);
		}
	}
	*/
	
}
