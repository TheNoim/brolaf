package Brolaf;

import java.awt.Font;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import de.slikey.effectlib.EffectLib;
import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.effect.TextLocationEffect;

public class Countdown {

	int ten = 21;
	private Brolaf plugin;
	private Set s;
	public Countdown(Brolaf plugin, Set s85){
		this.plugin = plugin;
		this.s = s;
		
	}
	
	@SuppressWarnings("deprecation")
	public void CountDown(final Player p) {
		Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&b[&4Brolaf&b] &6Der Countdown ist gestartet !"));
		ten = 21;
		this.plugin.count = true;
		for (Player online : Bukkit.getOnlinePlayers()){
			online.setGameMode(GameMode.ADVENTURE);
			online.removePotionEffect(PotionEffectType.SATURATION);
	    	online.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
	    	online.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20*20 , 127));
	    	online.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, 20*20 , 127));
	    	online.playEffect(online.getLocation(), Effect.MOBSPAWNER_FLAMES, 20);
		}
		Bukkit.getScheduler().scheduleAsyncRepeatingTask(this.plugin, new Runnable(){

			public void run() {
				
				EffectLib lib = EffectLib.instance();
				EffectManager em = new EffectManager(lib);
				World w = p.getWorld();
				if (ten >= 0){
					ten--;
				} else {		
					Countdown.this.plugin.count = false;
					Bukkit.getScheduler().cancelAllTasks();
				}
				if (ten == 20){
					
					double x = Countdown.this.s.cfg.getDouble("x");
					double y = Countdown.this.s.cfg.getDouble("y");
					double z = Countdown.this.s.cfg.getDouble("z");
					Location loc = new Location(w, y, z, x);
					Letter.drawString("20", Material.REDSTONE_BLOCK, (byte) 0, loc, Direction.SOUTH);
					Letter.centreString("20", Material.EMERALD_BLOCK, (byte) 1, loc, Direction.NORTH);
					TextLocationEffect effect = new TextLocationEffect(em, loc);
					effect.text = "20";
				    effect.start();
				    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&c&l20"));
				    for (Player online : Bukkit.getOnlinePlayers()){
				    	online.playNote(online.getLocation(),(byte) 1,(byte) 1);
				    }
				}
				if (ten == 10){
					
					double x = Countdown.this.s.cfg.getDouble("x");
					double y = Countdown.this.s.cfg.getDouble("y");
					double z = Countdown.this.s.cfg.getDouble("z");
					Location loc = new Location(w, y, z, x);
					Letter.drawString("10", Material.REDSTONE_BLOCK, (byte) 0, loc, Direction.SOUTH);
					TextLocationEffect effect = new TextLocationEffect(em, loc);
					effect.text = "10";
				    effect.start();
				    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&c&l10"));
				    for (Player online : Bukkit.getOnlinePlayers()){
				    	online.playNote(online.getLocation(),(byte) 1,(byte) 1);
				    }
				}
				if (ten == 5){
					
					double x = Countdown.this.s.cfg.getDouble("x");
					double y = Countdown.this.s.cfg.getDouble("y");
					double z = Countdown.this.s.cfg.getDouble("z");
					Location loc = new Location(w, y, z, x);
					Letter.drawString("5", Material.EMERALD, (byte) 0, loc, Direction.SOUTH);
					TextLocationEffect effect = new TextLocationEffect(em, loc);
					effect.text = "5";
				    effect.start();
				    
				    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&c&l5"));
				    for (Player online : Bukkit.getOnlinePlayers()){
				    	online.playNote(online.getLocation(),(byte) 1,(byte) 1);
				    }
				}
				if (ten == 4){
					
					double x = Countdown.this.s.cfg.getDouble("x");
					double y = Countdown.this.s.cfg.getDouble("y");
					double z = Countdown.this.s.cfg.getDouble("z");
					Location loc = new Location(w, y, z, x);
					Letter.drawString("4", Material.EMERALD, (byte) 0, loc, Direction.SOUTH);
					TextLocationEffect effect = new TextLocationEffect(em, loc);
					effect.text = "4";
				    effect.start();
				    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&c&l4"));
				    for (Player online : Bukkit.getOnlinePlayers()){
				    	online.playNote(online.getLocation(),(byte) 1,(byte) 1);
				    }
				}
				if (ten == 3){
					
					double x = Countdown.this.s.cfg.getDouble("x");
					double y = Countdown.this.s.cfg.getDouble("y");
					double z = Countdown.this.s.cfg.getDouble("z");
					Location loc = new Location(w, y, z, x);
					Letter.drawString("3", Material.EMERALD, (byte) 0, loc, Direction.SOUTH);
					TextLocationEffect effect = new TextLocationEffect(em, loc);
					effect.text = "3";
				    effect.start();
				    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&c&l3"));
				    for (Player online : Bukkit.getOnlinePlayers()){
				    	online.playNote(online.getLocation(),(byte) 1,(byte) 1);
				    }
				}
				if (ten == 2){
					
					double x = Countdown.this.s.cfg.getDouble("x");
					double y = Countdown.this.s.cfg.getDouble("y");
					double z = Countdown.this.s.cfg.getDouble("z");
					Location loc = new Location(w, y, z, x);
					Letter.drawString("2", Material.EMERALD, (byte) 0, loc, Direction.SOUTH);
					TextLocationEffect effect = new TextLocationEffect(em, loc);
					effect.text = "2";
				    effect.start();
				    
				    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&c&l2"));
				    for (Player online : Bukkit.getOnlinePlayers()){
				    	online.playNote(online.getLocation(),(byte) 1,(byte) 1);
				    }
				}
				if (ten == 1){
					
					double x = Countdown.this.s.cfg.getDouble("x");
					double y = Countdown.this.s.cfg.getDouble("y");
					double z = Countdown.this.s.cfg.getDouble("z");
					Location loc = new Location(w, y, z, x);
					Letter.drawString("1", Material.DIAMOND_BLOCK, (byte) 0, loc, Direction.SOUTH);
					TextLocationEffect effect = new TextLocationEffect(em, loc);
					effect.text = "1";
				    effect.start();
				    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&c&l1"));
				    for (Player online : Bukkit.getOnlinePlayers()){
				    	online.playNote(online.getLocation(),(byte) 1,(byte) 1);
				    }
				}
				if (ten == 0){
					
					double x = Countdown.this.s.cfg.getDouble("x");
					double y = Countdown.this.s.cfg.getDouble("y");
					double z = Countdown.this.s.cfg.getDouble("z");
					Location loc = new Location(w, y, z, x);
					Letter.drawString("START", Material.GOLD_BLOCK, (byte) 0, loc, Direction.SOUTH);
					TextLocationEffect effect = new TextLocationEffect(em, loc);
					effect.text = "START";
				    effect.start();
				    for (Player online : Bukkit.getOnlinePlayers()){
				    	online.playSound(online.getLocation(), Sound.SUCCESSFUL_HIT, 1, 1);
				    	online.setGameMode(GameMode.SURVIVAL);
				    	online.removePotionEffect(PotionEffectType.SATURATION);
				    	online.removePotionEffect(PotionEffectType.DAMAGE_RESISTANCE);
				    	online.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 601*20 , 127));
				    	online.addPotionEffect(new PotionEffect(PotionEffectType.SATURATION, 601*20 , 127));
				    	online.playEffect(online.getLocation(), Effect.MOBSPAWNER_FLAMES, 20);
				    	online.playEffect(online.getLocation(), Effect.MOBSPAWNER_FLAMES, 20);
				    	online.playEffect(online.getLocation(), Effect.MOBSPAWNER_FLAMES, 20);
				    	online.playEffect(online.getEyeLocation(), Effect.MOBSPAWNER_FLAMES, 20);
				    	online.playEffect(online.getEyeLocation(), Effect.MOBSPAWNER_FLAMES, 20);
				    	online.playEffect(online.getEyeLocation(), Effect.MOBSPAWNER_FLAMES, 20);
				    }
				    Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&c&lStart !"));
				    Countdown.this.plugin.count = false;
				    
				}
				
			}
			
		}, 0, 20L);
		
		
	}
	
	
	
}
